package com.shop.user.entity;

import java.io.Serializable;

import com.swak.entity.BaseEntity;

/**
 * 店铺
 * 
 * @author lifeng
 */
public class Store extends BaseEntity<Long> implements Serializable {

	private static final long serialVersionUID = 1L;
}