package com.shop.user.entity;

import java.io.Serializable;

import com.swak.entity.BaseEntity;

/**
 * 用户
 * 
 * @author lifeng
 */
public class User extends BaseEntity<Long> implements Serializable {

	private static final long serialVersionUID = 1L;
	
    private Byte type; // 用户类型
	private String headimg; // 管理员图像
	private String openId; // 微信openID
	
	public Long getUserId() {
		return userId;
	}
	public String getUserName() {
		return userName;
	}
	public Byte getType() {
		return type;
	}
	public void setType(Byte type) {
		this.type = type;
	}
	public String getHeadimg() {
		return headimg;
	}
	public void setHeadimg(String headimg) {
		this.headimg = headimg;
	}
	public String getOpenId() {
		return openId;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}
}