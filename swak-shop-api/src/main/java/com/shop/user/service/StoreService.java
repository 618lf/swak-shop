package com.shop.user.service;

import com.shop.user.entity.Store;
import com.swak.annotation.FluxAsync;

/**
 * 店铺服务
 * 
 * @author lifeng
 */
@FluxAsync
public interface StoreService {

	/**
	 * 获得店铺
	 * 
	 * @param id
	 * @return
	 */
	Store get(Long id);
}