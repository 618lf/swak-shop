package com.shop.user.service;

import com.shop.user.entity.User;
import com.swak.annotation.FluxAsync;

/**
 * 用户服务
 * 
 * @author lifeng
 */
@FluxAsync
public interface UserService {
   
	/**
	 * 获得用户
	 * 
	 * @param id
	 * @return
	 */
	User get(Long id);
}