import Vue from 'vue'
import Router from 'vue-router'

const _import = require('./_import')

/* layout */
import Layout from '../views/layout/Layout'

Vue.use(Router)

/**
 * icon : the icon show in the sidebar
 * hidden : if `hidden:true` will not show in the sidebar
 * redirect : if `redirect:noredirect` will not redirct in the levelbar
 * noDropdown : if `noDropdown:true` will not has submenu in the sidebar
 * meta : `{ role: ['admin'] }`  will control the page role
 **/
export const constantRouterMap = [
  {path: '/login', component: _import('login'), hidden: true},
  {path: '/logout', component: _import('login'), hidden: true},
  {
    path: '/error', component: Layout, hidden: true,
    redirect: '404',
    children: [{
      path: '404', name: '404', component: _import('error/404'), hidden: true,
    }]
  },
  {
    path: '/',
    component: Layout,
    redirect: '/admin',
    children: [{
      path: '/admin', name: 'admin', component: _import('index'), meta: {title: '首页'},
    }]
  },
  {
    path: '/page/',
    component: Layout,
    redirect: '/page/tables',
    children: [{
      path: 'tables', name: 'tables', component: _import('pages/Tables'), meta: {title: 'Tables'},
    },{
      path: 'forms', name: 'forms', component: _import('pages/Forms'), meta: {title: 'Forms'},
    },{
      path: 'tree_tables', name: 'treeTables', component: _import('pages/TreeTables'), meta: {title: 'TreeTables'},
    },{
      path: 'tree_list', name: 'treeList', component: _import('pages/TreeList'), meta: {title: 'TreeList'},
    },{
      path: 'left-right', name: 'leftRight', component: _import('pages/LeftRight'), meta: {title: 'LeftRight'},
    }]
  },
  {path: '*', redirect: '/error/404', hidden: true}
]

export const asyncRouterMap = [
  {
    path: '/admin/system',
    name: 'system',
    component: Layout,
    redirect: 'noredirect',
    meta: {title: '系统管理'},
    children: [{
      path: 'attachment',
      name: 'attachment',
      meta: {title: '图库'},
      component: _import('system/menu/list'),
    }, {
      path: 'menu/list',
      name: 'menu',
      meta: {title: '菜单管理'},
      component: _import('system/menu/list'),
    }, {
      path: 'menu/add',
      name: 'menu_dd',
      meta: {title: '添加菜单'},
      component: _import('system/menu/add'),
    }, {
      path: 'area/list',
      name: 'area',
      meta: {title: '区域管理'},
      component: _import('system/menu/list'),
    }, {
      path: 'office/list',
      name: 'office',
      meta: {title: '组织管理'},
      component: _import('system/menu/list'),
    }]
  },
]

export default new Router({
  mode: 'hash',
  scrollBehavior: () => ({y: 0}),
  routes: constantRouterMap.concat(asyncRouterMap)
})