import fetch from '../fetch'

const Captcha = {
  image: () => fetch({
    responseType: 'blob',
    url: '/api/captcha/image',
    method: 'get'
  }),
  base64: () => fetch({
    url: '/api/captcha/base64',
    method: 'get'
  }),
  check: (captcha, captcha_etag) => fetch({
    headers: {'captcha-etag': captcha_etag},
    url: '/api/captcha/check',
    method: 'post',
    data: {
      captcha: captcha
    }
  }),
}

export default Captcha