import fetch from '../fetch'

const Menu = {
  list: () => fetch({
    url: '/admin/system/menu/page',
    method: 'get'
  }),
}

export default Menu