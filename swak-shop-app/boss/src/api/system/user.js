import fetch from '../fetch'

const User = {
  login: (username, password, captcha) => fetch({
    url: '/api/login',
    method: 'post',
    data: {
      username,
      password,
      captcha
    }
  }),
  logout: () => fetch({
    url: '/api/logout',
    method: 'post'
  }),
  getUser: () => fetch({
    url: '/api/system/self/me',
    method: 'get'
  }),
  getMenus: () => fetch({
    url: '/api/system/self/menu',
    method: 'get'
  }),
}

export default User