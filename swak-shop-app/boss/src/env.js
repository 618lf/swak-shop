/**
 * 配置编译环境和线上环境之间的切换
 */
const env = process.env

/**
 *
 */
const Site = {
  title: "Avue",
  logo: "A",
  author: "BY smallwei",
  whiteList: ["/login", "/404", "/401", "/lock"],
  lockPage: '/lock',
  info: {
    title: "Avue 通用管理系统快速开发框架",
    list: [
      'Avue 是一个基于vue+vuex+vue-router快速后台管理模板，采用token交互验证方式。',
      '您可以 Avue 为基础，不只限制于vue的页面，你可以嵌入任意第三方网站，基于iframe框架。',
      'Avue 构建简单上手快，最大程度上帮助企业节省时间成本和费用开支。',
    ]
  },
}

export {
  Site,
  env
}