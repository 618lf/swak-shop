import router from './router'
import store from './store'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import {getToken} from './api/auth'

// add tag
function addTag(to) {
  //store.commit('ADD_TAG', {
   // label: to.meta.title,
  //  value: to.path,
  //});
}

router.beforeEach((to, from, next) => {
  NProgress.start()
  if (to.path === '/login') {
    if (getToken()) {
      next({path: '/'})
      NProgress.done()
    } else {
      next()
    }
  } else if (to.path === '/logout') {
    store.dispatch('LogOut').then(() => {
      next('/login')
      NProgress.done()
    })
  } else if (getToken()) {
    if (!store.getters.user.info) {
      store.dispatch('GetUser').then(() => {
        addTag(to);
        next({...to})
      });
    } else {
      addTag(to);
      next();
    }
  } else {
    next('/login')
    NProgress.done()
  }
})

router.afterEach(() => {
  NProgress.done()
})
