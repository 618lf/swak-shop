import {getStore, removeStore, setStore} from '@/ui/common/helper/store'
import {validatenull} from '@/ui/common/helper/validate'
import {baseUrl, Site} from '@/env';

const common = {
  state: {
    config: {
      isCollapse: false,
      isFullScren: false,
      isLock: getStore({
        name: 'isLock'
      }) || false,
      theme: getStore({
        name: 'theme'
      }) || '#409EFF',
      lockPasswd: getStore({
        name: 'lockPasswd'
      }) || '',
    },
    site: Site,
  },
  actions: {
    //获取字典公用类
    GetDic({commit, state, dispatch}, dic) {
      return new Promise((resolve, reject) => {
        if (dic instanceof Array) {
          Promise.all(dic.map(ele => getDic(ele))).then(data => {
            let result = {};
            dic.forEach((ele, index) => {
              result[ele] = data[index].data;
            })
            resolve(result)
          })
        }
      })
    }
  },
  mutations: {
    SET_COLLAPSE: (state, action) => {
      state.config.isCollapse = !state.config.isCollapse;
    },
    SET_FULLSCREN: (state, action) => {
      state.config.isFullScren = !state.config.isFullScren;
    },
    SET_LOCK: (state, action) => {
      state.config.isLock = true;
      setStore({
        name: 'isLock',
        content: state.config.isLock,
        type: 'session'
      })
    },
    SET_THEME: (state, color) => {
      state.config.theme = color;
      setStore({
        name: 'theme',
        content: state.config.theme,
      })
    },
    SET_LOCK_PASSWD: (state, lockPasswd) => {
      state.config.lockPasswd = lockPasswd;
      setStore({
        name: 'lockPasswd',
        content: state.config.lockPasswd,
        type: 'session'
      })
    },
    CLEAR_LOCK: (state, action) => {
      state.config.isLock = false;
      state.config.lockPasswd = '';
      removeStore({
        name: 'lockPasswd'
      });
      removeStore({
        name: 'isLock'
      });
    },
  }
}
export default common
