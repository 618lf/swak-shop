const getters = {
  token: state => state.user.token,
  user: state => state.user.user,
  site: state => state.common.site,
  config: state => state.common.config,
  tags: state => state.tags,
}
export default getters
