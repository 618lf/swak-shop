const Mock = require('mockjs');

// 获取 mock.Random 对象
const Random = Mock.Random;

const userData = function () {
  return {
    obj: {
      id: Random.id(),
      name: Random.cname(),
      no: Random.string('0123456789', 8),
      image: Random.dataImage('300x250', 'mock的图片'),
    },
    code: 1,
    token: '1'
  }
}

/**
 *
 * @returns {{}}
 */
const menuList = function () {
  return {
    obj: [
      {
        id: 1,
        parent: 0,
        name: 'IMAGES',
        type: 1,
        href: '/',
        iconClass: 'iconfont icon-product',
      },
      {
        id: 2,
        parent: 0,
        name: 'UI',
        type: 1,
        href: '/',
        iconClass: 'iconfont icon-product',
      },
      {
        id: 21,
        parent: 2,
        name: '列表',
        type: 2,
        href: '/page/tables',
        iconClass: 'iconfont icon-product',
      },
      {
        id: 22,
        parent: 2,
        name: '表单',
        type: 2,
        href: '/page/forms',
        iconClass: 'iconfont icon-product',
      },
      {
        id: 23,
        parent: 2,
        name: 'TreeTables',
        type: 2,
        href: '/page/tree_tables',
        iconClass: 'iconfont icon-product',
      },
      {
        id: 24,
        parent: 2,
        name: '其他',
        type: 2,
        href: '/',
        iconClass: 'iconfont icon-product',
      },
      {
        id: 3,
        parent: 0,
        name: 'Three',
        type: 1,
        href: '/',
        iconClass: 'iconfont icon-product',
        hasGroup: true,
      },
      {
        id: 31,
        parent: 3,
        name: 'Three1',
        type: 1,
        href: '/',
        iconClass: 'iconfont icon-product',
      },
      {
        id: 311,
        parent: 31,
        name: 'Three11',
        type: 2,
        href: '/',
        iconClass: 'iconfont icon-product',
      },
      {
        id: 312,
        parent: 31,
        name: 'Three12',
        type: 2,
        href: '/',
        iconClass: 'iconfont icon-product',
      }
    ],
    code: 1,
    token: '1'
  }
}

Mock.setup({timeout: 200 - 4000});

// api mock
Mock.mock('/admin/login', 'post', userData);
Mock.mock('/admin/system/self/me', 'get', userData);
Mock.mock('/admin/system/menu/page', 'get', menuList);