import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
import {Style} from './ui'
import locale from 'element-ui/lib/locale/lang/en'
import './ui/misc/dropdown'
import '@/permission'

require('./mock.js')

Vue.use(ElementUI, {locale})
Vue.use(Style)
Vue.config.productionTip = false

new Vue({
  store,
  render: h => h(App),
  router,
}).$mount('#app')
