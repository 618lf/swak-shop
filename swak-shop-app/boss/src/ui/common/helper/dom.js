export const triggerEvent = function (el, name, data) {
  var evt = document.createEvent('Event')
  evt.initEvent(name, true, true)
  data && Object.assign(evt, data)
  el.dispatchEvent(evt)
}

export const siblings = function (elem) {
  var n = (elem.parentNode || {}).firstChild
  var matched = []

  for (; n; n = n.nextSibling) {
    if (n.nodeType === 1 && n !== elem) {
      matched.push(n)
    }
  }

  return matched
}

/* 匹配最近的祖先节点 (使用原生方法) */
export const closest = function (el, selector, ctx = document) {
  var matchesSelector = el.matches || el.webkitMatchesSelector || el.mozMatchesSelector || el.msMatchesSelector
  if (!matchesSelector) {
    return _closest(el, selector, ctx)
  }
  do {
    if (el === ctx || matchesSelector.call(el, selector)) {
      return el
    }
  } while (el = el.parentElement)
}

/* 判断元素是否匹配给定的CSS选择器 (使用原生方法) */
export const matches = function (el, selector) {
  var matchesSelector = el.matches || el.webkitMatchesSelector || el.mozMatchesSelector || el.msMatchesSelector
  if (!matchesSelector) {
    return _matches(el, selector)
  }
  return matchesSelector.call(el, selector)
}

/* 匹配最近的祖先节点 */
export const _closest = function (el, selector, ctx = document) {
  if (!el) return
  do {
    if (el === ctx || _matches(el, selector)) {
      return el
    }
  } while (el = el.parentNode)
}

/* 判断元素是否匹配给定的CSS选择器 */
export const _matches = function (el, selector) {
  if (!el) return false
  selector = selector.split('.')
  var tag = selector.shift().toUpperCase(),
    re  = new RegExp('\\s(' + selector.join('|') + ')(?=\\s)', 'g')
  return (
    (tag === '' || el.nodeName.toUpperCase() === tag) &&
    (!selector.length || ((' ' + el.className + ' ').match(re) || []).length === selector.length)
  )
}