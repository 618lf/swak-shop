import {
  Style,
} from './module'

const version = '1.0'
const components = [
  Style,
]

function install(Vue) {
  if (install.installed) {
    return
  }
  install.installed = true
  components.forEach((Component) => {
    Component.install(Vue)
  })
}

if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.use(install)
}

export {
  install,
  version,
  Style,
}

export default {
  install,
  version
}
