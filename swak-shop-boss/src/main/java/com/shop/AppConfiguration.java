package com.shop;

import java.util.Set;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.swak.config.flux.SecurityConfigurationSupport;
import com.swak.utils.Sets;
import com.swak.vertx.config.IRouterConfig;
import com.swak.vertx.config.VertxProperties;
import com.swak.vertx.security.SecurityHandler;

import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;

/**
 * 项目配置
 * 
 * @author lifeng
 * @date 2020年4月4日 下午12:03:35
 */
@Configuration
public class AppConfiguration {

    /**
     * 安全过滤
     *
     * @return SecurityConfigurationSupport
     */
    @Bean
    public SecurityConfigurationSupport securitySupport() {
        // 权限配置
        SecurityConfigurationSupport support = new SecurityConfigurationSupport();
        support.definition("/api/anno=anno")
               .definition("/api/user/=user")
               .definition("/api/admin/=user, role[admin]")
               .definition("/=anno");
        return support;
    }

    /**
     * 网站自定义的路由配置， 可以配置多个
     *
     * @return IRouterConfig
     */
    @Bean
    public IRouterConfig routerConfig(SecurityHandler handler, VertxProperties properties) {
        return (vertx, router) -> {
            Set<String> headers = Sets.newHashSet();
            headers.add("X-Requested-With");
            headers.add(properties.getJwtTokenName());
            router.route().handler(CorsHandler.create("*").allowedHeaders(headers));
            router.route()
                    .handler(BodyHandler.create(properties.getUploadDirectory())
                            .setBodyLimit(properties.getBodyLimit())
                            .setDeleteUploadedFilesOnEnd(properties.isDeleteUploadedFilesOnEnd()));
            router.route().handler(handler);
        };
    }
}