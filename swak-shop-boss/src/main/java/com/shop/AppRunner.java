package com.shop;

import com.swak.Application;
import com.swak.ApplicationBoot;

/**
 * 定义系统启动类
 * 
 * @author lifeng
 * @date 2020年4月4日 下午12:03:14
 */
@ApplicationBoot
public class AppRunner {

	public static void main(String[] args) {
		Application.run(AppRunner.class, args);
	}
}